

//thêm sửa xóa frontend ở đây , sau sẽ có RestAPI ajax thì thêm code vào phần success ở ajax

//thêm nhân viên
function addRow() {
    if (!checkEmptyInput()) {
        var name = $('#name').val(),
            phone = $('#phone').val(),
            email = $('#email').val(),
            address = $('#address').val();
        // thêm ở phía client
        var table = $('#dataTables-example').DataTable();
        table.row.add([
            "100(id fake, xử lí sau )",
            name,
            phone,
            email,
            address,
            "<button class='btn btn-outline-info btn-rounded'" +
            " onclick='openNavToEdit(this);'><i class='fas fa-pen'></i></button>" +
            " <button class='btn btn-outline-danger btn-rounded'" +
            "onclick='removeRow(this);'><i class='fas fa-trash'></i></button>"
        ]).draw(false);
        closeNav();
    }
}

// xóa nhân viên khi nhấn button xóa (onclick)
function removeRow(button) {
    // xóa luôn ở phía client
    var table = $('#dataTables-example').DataTable();
    table.row($(button).parents('td').parents('tr')).remove().draw();
}

var rowIndexEdit, idEdit;
//sửa nhân viên
function editRow() {
    // sửa = ở phía client
    var table = $('#dataTables-example').DataTable();

    if (!checkEmptyInput()) {
        var name = $('#name').val(),
        phone = $('#phone').val(),
        email = $('#email').val(),
        address = $('#address').val();
        // sửa lại bảng
        table.row(rowIndexEdit).data([idEdit,
            name,
            phone,
            email,
            address,
            "<button class='btn btn-outline-info btn-rounded'" +
            " onclick='openNavToEdit(this);'><i class='fas fa-pen'></i></button>" +
            " <button class='btn btn-outline-danger btn-rounded'" +
            "onclick='removeRow(this);'><i class='fas fa-trash'></i></button>"]).draw();

        closeNav();
    }
}


// ---------------------------------------------------------------------------------------------------------------------
// các hàm phục vụ cho việc check dữ liệu, clear dữ liệu khi bật FormData,
// đóng mở form ,dán dữ liệu từ hàng vào form,................ ở đây nhé !
// ---------------------------------------------------------------------------------------------------------------------

// load thông tin từ hàng vào form khi nhấn icon sửa (hình bút !)
function selectedRowToInput() {
    var table = document.getElementById("dataTables-example");
    for (var i = 1; i < table.rows.length; i++) {
        table.rows[i].onclick = function () {
            // get the seected row index
            document.getElementById("customer_id").value = this.cells[0].innerHTML;
            document.getElementById("name").value = this.cells[1].innerHTML;
            document.getElementById("phone").value = this.cells[2].innerHTML;
            document.getElementById("email").value = this.cells[3].innerHTML;
            document.getElementById("address").value = this.cells[4].innerHTML;
        };
    }
}

// clear data form khi bật nút add user !
function removedRowToInput() {
    document.getElementById("name").value = "";
    document.getElementById("phone").value = "";
    document.getElementById("email").value = "";
    document.getElementById("address").value = "";
}
// check the empty input
function checkEmptyInput() {
    var isEmpty = false,
        name = document.getElementById("name").value,
        phone = document.getElementById("phone").value,
        phone = document.getElementById("email").value,
        address = document.getElementById("address").value;
    if (name === "") {
        alert("name Name Cannot Be Empty");
        isEmpty = true;
    } else if (phone === "") {
        alert("phone Cannot Be Empty");
        isEmpty = true;
    } else if (email === "") {
        alert("email cannot Be Empty");
        isEmpty = true;
    } else if (address === "") {
        alert("address cannot Be Empty");
        isEmpty = true;
    }
    return isEmpty;
}

// hàm mở form
function openNav() {
    //hàm xóa sạch thông tin khi mở form thêm mới !
    removedRowToInput();
    document.getElementById("myForm").style.height = "90%";
    document.getElementById("overlay2").style.display = "block";
    // vô hiệu hóa nút sửa vì ta đang cần thêm !
    document.getElementById("editRow").style.display = "none";
    document.getElementById("addRow").style.display = "inline-block";
}

// hàm đóng form(dùng cho các nút X, thoát)
function closeNav() {
    document.getElementById("myForm").style.height = "0%";
    document.getElementById("overlay2").style.display = "none";
}

// mở form để edit
function openNavToEdit(button) {
    document.getElementById("myForm").style.height = "90%";
    document.getElementById("overlay2").style.display = "block";
    // vô hiệu hóa nút thêm vì ta đang cần sửa !
    document.getElementById("editRow").style.display = "inline-block";
    document.getElementById("addRow").style.display = "none";
    //hàm đưa thông tin từ hàng vào form ! 
    selectedRowToInput();

    var table = $('#dataTables-example').DataTable();
    //lấy ra hàng
    rowIndexEdit = table.row($(button).parents('td').parents('tr')).index();
    // lấy chỉ mục dòng
    //var rowNumber = row.rowIndex;
    // lấy giá trị trong cột 0:id
    idEdit = table.row(rowIndexEdit).data()[0];

    console.log("chỉ mục hàng = " + rowIndexEdit);
    console.log("chỉ mục cột id = " + idEdit);
}

